
# As per official Python doc 'https://docs.python.org/3/tutorial/introduction.html'

# String literals can span multiple lines. One way is using triple-quotes: """...""" or '''...'''. End of lines
# are automatically included in the string, but it’s possible to prevent this by adding a (\) at the end of the line.





print("""No Gap""")
print("""
    gap is present for the first and last empty lines
    """) # empty first line is printed 
print("""\
By using escape character option, first line is ignored as it is empty
    """) # will not print first empty line

print("""How to ignore the last line?
      \\""") # giving twice \\ didn't worked

print("""\
    By using the escape character option the first line is ignored\
    """) # Why the first one should be in the first line last one last before one??
