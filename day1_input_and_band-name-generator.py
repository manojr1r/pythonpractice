import re 

# input('What is your name? ')
name =input('What is your name? ')
#1. Create a greeting for your program.
print(name.capitalize() + " Welcome to band name generator")
#2. Ask the user for the city that they grew up in.
city= input("What is the city you grew up in?\n")

#3. Ask the user for the name of a pet.
pet= input("What is the name of your pet?\n")
#4. Combine the name of their city and pet and show them their band name.
print("Here is the band name that suggest: "+ city + pet.capitalize())

#5. Make sure the input cursor shows on a new line:
# Solution: https://replit.com/@appbrewery/band-name-generator-end


# print(camelcase(pet))

#Observations: .upper and camelcase commands doesn't work