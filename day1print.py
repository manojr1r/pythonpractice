#Day1 out of 100 
print("hello world")

#Exercise day 1
print("Day 1 - Python Print Function\n"
"The function is declared like this:\n"
"print('what to print')")

print("                ")

#from Python document

print('string '
'multi liner in code, but will print in the same line at output')

print("                                     ")
print("   for \"\"\"                                  ")

print("""for string literals with ''' as text is new line output will be same
as this is a not as raw string, will print in a different line in output""") #\n will be treated as a special character
print("                                     ")
print("""\for string literals with \''' escape character and enter both work!! and add extra space line in between lines\n
\This line will print after a gap in line at output""") #\n will be treated as a special character

print("                                     ")
print("       for  rawstring                           ")

'#for use r for Raw string'
print(r'for raw string  new line command is ignored\n'
      ' will treat as an escape character in this line \n'
      'comes in new line')
