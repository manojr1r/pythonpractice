from requests import options
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options

options = webdriver.ChromeOptions()
options.add_experimental_option("detach", True)  #This will not close the browser automatically after execution
options.add_argument('start-maximized')

options.add_experimental_option("excludeSwitches",["enable-automation", "enable-logging"])

service_obj = Service(r'chromedriver.exe')
driver = webdriver.Chrome(service=service_obj, options=options,)

driver.get("https://google.com")
