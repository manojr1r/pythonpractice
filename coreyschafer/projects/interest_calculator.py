principle = 0
rate = 0
time = 0

while principle <=0:
    principle = float(input("Enter the principle amount: "))
    if principle < 0:
        print("Principle can't be less than zero")

rate = float(input("Enter the interest rate: "))
if rate < 0:
    print("Interest rate can't be less than zero")