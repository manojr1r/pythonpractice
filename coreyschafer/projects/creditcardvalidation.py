import re

#ask for the credit card and validate length and regex
# user_input = None
# break_loop = False
# while True:
    # if break_loop:
        # break
user_input = (input("Enter your credit card: "))
    # user_input = user_input.replace(" ", "")
    # if len(user_input) == 16:
    #     pattern = re.compile(r'\d{16}')
    #     matches = pattern.finditer(user_input)
    #     for match in matches:
    #         break_loop = True
    #         break

# step 2 add all digits in the odd places from right to left
reversed_odd_digit_sum = 0
for i in (user_input[-1::-2]):
    reversed_odd_digit_sum += int(i)

# Double every second digit fom right to left
reversed_odd_digit_double = 0
for i in reversed(user_input[::2]):
    i = int(i)*2
    if i > 9:
        reversed_odd_digit_double += (1 + (i%10))
    else:
        reversed_odd_digit_double += i

# add both the digits
total = reversed_odd_digit_sum + reversed_odd_digit_double

# if divisble by 10 then it is a valid number
if total % 10 == 0:
    print("credit no. validated")
else:
    print("In-Valid")